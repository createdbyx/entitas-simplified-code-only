# README #

This project is a fork of the Entitas project located on github. https://github.com/sschmid/Entitas-CSharp

It is the union of all three Entitas Unity projects combined into one code base containing only the code files. Making it very easy to add the core Entitas code files as a submodule/subtree inside your unity project repos. 